package com.pawel.nowak.hybris.blog.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



import org.springframework.web.servlet.ModelAndView;

import com.pawel.nowak.hybris.blog.model.Comment;
import com.pawel.nowak.hybris.blog.services.CommentsService;

@Controller
public class CommentsController {
	
	CommentsService commentsService;
	
	@Autowired
	public void setCommentsService(CommentsService commentsService) {
		this.commentsService = commentsService;
	}

	@RequestMapping(value="doaddcomment", method=RequestMethod.POST)
	public ModelAndView commentAdded (Model model, @Valid Comment comment, BindingResult result ) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		comment.setAuthor(userDetails.getUsername());
		
		if (result.hasErrors()) {
			return new ModelAndView("redirect:/article/" + comment.getArticleId());
		}
		else {
			commentsService.addNewComment(comment);
			return new ModelAndView("redirect:/");
		}
	}
}
