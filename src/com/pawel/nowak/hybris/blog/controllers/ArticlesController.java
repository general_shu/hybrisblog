package com.pawel.nowak.hybris.blog.controllers;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pawel.nowak.hybris.blog.model.Article;
import com.pawel.nowak.hybris.blog.model.Comment;
import com.pawel.nowak.hybris.blog.model.DateRange;
import com.pawel.nowak.hybris.blog.services.ArticlesService;
import com.pawel.nowak.hybris.blog.services.CommentsService;

@Controller
public class ArticlesController {
	private ArticlesService articlesService;
	private CommentsService commentsService;

	@Autowired
	public void setArticlesService(ArticlesService articlesService) {
		this.articlesService = articlesService;
	}
	
	@Autowired
	public void setCommentsService(CommentsService commentsService) {
		this.commentsService = commentsService;
	}

	@RequestMapping("/")
	public String showHome(Model model) {
		List<Article> articles = articlesService.getCurrent();
			
		model.addAttribute("articles", articles);
		model.addAttribute("dateRange", new DateRange());

		return "home";
	}
	
	@RequestMapping("addarticle")
	public String createNewArticle (Model model) {
		
		model.addAttribute("article", new Article());
		
		return "addarticle";
	}
	
	@RequestMapping(value="doadd", method=RequestMethod.POST)
	public String articleAdded (Model model, @Valid Article article, BindingResult result) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		article.setAuthor(userDetails.getUsername());
		
		if (result.hasErrors()) {
			return "addarticle";
		}
		else {
			articlesService.addNewArticle(article);
			return "doadd";
		}
	}
	
	@RequestMapping(value="/article/{id}", method=RequestMethod.GET)
	public ModelAndView showArticle(@PathVariable int id) {
		
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("article");
	    
	    Article article = articlesService.getArticleById(id);
	    List<Comment> comments = commentsService.getCommentsForArticle(id);
	    
	    modelAndView.addObject("comments", comments);
	    modelAndView.addObject("article", article);
	    modelAndView.addObject("comment", new Comment());
	    
	    return modelAndView;
	}

	
	@RequestMapping(value="/filter", method=RequestMethod.POST)
	public String showFilterResults(@Valid DateRange dateRange, Model model) {
		List<Article> articles = articlesService.getCurrentBetweenTime(dateRange);

		model.addAttribute("articles", articles);
		
		return "filter";
	}
	
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public String showSearchResults(@Valid String keyWords, Model model) {
		List<Article> articles = articlesService.getCurrentByKeyWords(keyWords);
		
		model.addAttribute("articles", articles);
		
		return "search";
	}
	
	@RequestMapping("/managearticles")
	public String showManageArticles(Model model) {
		List<Article> articles = articlesService.getCurrent();
		
		model.addAttribute("articles", articles);
		
		return "managearticles";
	}
	
	@RequestMapping(value="/deletearticle/{id}", method=RequestMethod.GET)
	public ModelAndView showDeleteArticle(@PathVariable int id) {
		
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("deletearticle");
	    
	    this.articlesService.deleteArticleById(id);
	    this.commentsService.deleteByArticleId(id);
	    
	    return modelAndView;
	}
	
	@RequestMapping(value="/updatearticle/{id}", method=RequestMethod.GET)
	public ModelAndView showUpdateArticle(@PathVariable int id) {
		
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("updatearticle");
	    
	    Article article = this.articlesService.getArticleById(id);
	    String date = new SimpleDateFormat("MM/dd/yyyy").format(article.getDate());
	    
	    modelAndView.addObject("article", article);
	    modelAndView.addObject("articleDate", date);
	    
	    return modelAndView;
	}
	
	@RequestMapping(value="/doupdate", method=RequestMethod.POST)
	public ModelAndView showDoUpdateArticle(@Valid Article updatedArticle) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("home");
	    
	    updatedArticle.setAuthor(userDetails.getUsername());
	    this.articlesService.updateArticleById(updatedArticle);
	    
	    return modelAndView;
	}
}
