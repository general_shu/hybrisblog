package com.pawel.nowak.hybris.blog.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pawel.nowak.hybris.blog.model.User;
import com.pawel.nowak.hybris.blog.services.UsersService;

@Controller
public class UsersController {

	private UsersService usersService;
	
	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	@RequestMapping("/login")
	public String showLogin() {
			return "login";		
	}
	
	@RequestMapping("/register")
	public String showRegisteration(Model model) {
		
		model.addAttribute("user", new User());
		return "register";
	}
	
	@RequestMapping(value="/addaccount", method=RequestMethod.POST)
	public String createAccount(@Valid User user, BindingResult result) {
		
		if(result.hasErrors()) {
			return "register";
		}
		
		user.setEnabled(true);
		user.setAuthority("ROLE_USER");
		
		if (usersService.exists(user.getUsername())) {
			result.rejectValue("username", "DuplicateKey.user.username", "This username already exists.");
			return "register";
		}
		
		try {
			usersService.create(user);
		}
		catch (DuplicateKeyException e) {
			result.rejectValue("username", "DuplicateKey.user.username", "This username already exists.");
			return "register";
		}
		
		return "accountcreated";
	}
	
	@RequestMapping("/loggedout")
	public String showLoggedOut() {
		return "loggedout";
	}
	
	@RequestMapping("/admin")
	public String showAdmin() {
		return "admin";
	}
}
