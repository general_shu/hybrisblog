package com.pawel.nowak.hybris.blog.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.pawel.nowak.hybris.blog.model.Article;
import com.pawel.nowak.hybris.blog.model.DateRange;

@Component("articlesDao")
public class ArticlesDao {

	private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}
	
	public List<Article> getArticles() {

		return jdbc.query("select * from articles order by date desc", new RowMapper<Article>() {

			public Article mapRow(ResultSet rs, int rowNum) throws SQLException {
				Article article = new Article();
				
				article.setId(rs.getInt("id"));
				article.setAuthor(rs.getString("author"));
				article.setDate(rs.getDate("date"));
				article.setTitle(rs.getString("title"));
				article.setText(rs.getString("text"));
				article.setKeyWords(rs.getString("keywords"));

				return article;
			}

		});
	}
	
	public List<Article> getArticlesBetweenTime(DateRange dateRange) {

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("startingdate", dateRange.getStartingDate());
		params.addValue("endingdate", dateRange.getEndingDate());
		
		return jdbc.query("select * from articles where date between :startingdate and :endingdate order by date desc", params, new RowMapper<Article>() {

			public Article mapRow(ResultSet rs, int rowNum) throws SQLException {
				Article article = new Article();
				
				article.setId(rs.getInt("id"));
				article.setAuthor(rs.getString("author"));
				article.setDate(rs.getDate("date"));
				article.setTitle(rs.getString("title"));
				article.setText(rs.getString("text"));

				return article;
			}

		});
	}
	
	public boolean update(Article article) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(article);
		
		return jdbc.update("update articles set author=:author, date=:date, title=:title, text=:text, keywords=:keyWords where id=:id", params) == 1;
	}
	
	public boolean create(Article article) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(article);
		
		return jdbc.update("insert into articles (author, date, title, text, keywords) values (:author, :date, :title, :text, :keyWords)", params) == 1;
	}
	
	@Transactional
	public int[] create(List<Article> articles) {
		
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(articles.toArray());
		
		return jdbc.batchUpdate("insert into articles (author, date, title, text) values (:author, :date, :title, :text)", params);
	}
	
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);
		
		return jdbc.update("delete from articles where id=:id", params) == 1;
	}

	public Article getArticle(int id) {

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);

		return jdbc.queryForObject("select * from articles where id=:id", params,
				new RowMapper<Article>() {

					public Article mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Article article = new Article();

						article.setId(rs.getInt("id"));
						article.setAuthor(rs.getString("author"));
						article.setDate(rs.getDate("date"));
						article.setTitle(rs.getString("title"));
						article.setText(rs.getString("text"));
						article.setKeyWords(rs.getString("keywords"));

						return article;
					}

				});
	}

	public List<Article> getArticlesByKeyWord(String keyWords) {
		
		return jdbc.query("select * from articles where keywords like '%" + keyWords + "%' order by date desc", new RowMapper<Article>() {

			public Article mapRow(ResultSet rs, int rowNum) throws SQLException {
				Article article = new Article();
				
				article.setId(rs.getInt("id"));
				article.setAuthor(rs.getString("author"));
				article.setDate(rs.getDate("date"));
				article.setTitle(rs.getString("title"));
				article.setText(rs.getString("text"));
				System.out.println(article.toString());
				return article;
			}

		});
	}
}
