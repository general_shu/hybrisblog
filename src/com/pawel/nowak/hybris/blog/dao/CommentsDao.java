package com.pawel.nowak.hybris.blog.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.pawel.nowak.hybris.blog.model.Comment;

@Component("commentsDao")
public class CommentsDao {
	private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}
	
	public List<Comment> getCommentsByArticleId(int id) {

		MapSqlParameterSource params = new MapSqlParameterSource();
		
		params.addValue("articleid", id);
		
		return jdbc.query("select * from comments where articleid=:articleid", params, new RowMapper<Comment>() {

			public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
				Comment comment = new Comment();
				
				comment.setArticleId(rs.getInt("articleid"));
				comment.setAuthor(rs.getString("author"));
				comment.setDate(rs.getDate("date"));
				comment.setText(rs.getString("text"));

				return comment;
			}

		});
	}

	public boolean create(Comment comment) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(comment);
		
		return jdbc.update("insert into comments (author, date, text, articleid) values (:author, :date, :text, :articleId)", params) == 1;
	}

	public boolean deleteByArticleId(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);
		
		return jdbc.update("delete from comments where articleid=:id", params) == 1;
	}
}
