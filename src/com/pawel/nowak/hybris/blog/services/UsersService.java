package com.pawel.nowak.hybris.blog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pawel.nowak.hybris.blog.dao.UsersDao;
import com.pawel.nowak.hybris.blog.model.User;

@Service("usersService")
public class UsersService {

	private UsersDao usersDao;
	
	@Autowired
	public void setUsersDAO(UsersDao usersDAO) {
		this.usersDao = usersDAO;
	}

	public void create(User user) {
		usersDao.create(user);
	}

	public boolean exists(String username) {
		return usersDao.exists(username);
	}
}

