package com.pawel.nowak.hybris.blog.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.pawel.nowak.hybris.blog.dao.ArticlesDao;
import com.pawel.nowak.hybris.blog.model.Article;
import com.pawel.nowak.hybris.blog.model.DateRange;

@Service("articlesService")
public class ArticlesService {
	
	private ArticlesDao articlesDao;

	@Autowired
	public void setArticlesDao(ArticlesDao articlesDao) {
		this.articlesDao = articlesDao;
	}
	
	public List<Article> getCurrent() {
		return this.articlesDao.getArticles();
	}
	
	public List<Article> getCurrentBetweenTime(DateRange dateRange) {
		return this.articlesDao.getArticlesBetweenTime(dateRange);
	}

	@Secured("ROLE_ADMIN")
	public void addNewArticle(Article article) {
		this.articlesDao.create(article);
		
	}

	public Article getArticleById(int id) {
		return this.articlesDao.getArticle(id);
	}

	public List<Article> getCurrentByKeyWords(String keyWords) {
		return this.articlesDao.getArticlesByKeyWord(keyWords);
	}
	
	public boolean deleteArticleById(int id) {
		return this.articlesDao.delete(id);
	}

	public boolean updateArticleById(Article article) {
		return this.articlesDao.update(article);
	}
}
