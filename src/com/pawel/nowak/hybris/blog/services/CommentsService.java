package com.pawel.nowak.hybris.blog.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pawel.nowak.hybris.blog.dao.CommentsDao;
import com.pawel.nowak.hybris.blog.model.Comment;

@Service("commentsService")
public class CommentsService {

	private CommentsDao commentsDao;

	@Autowired
	public void setCommentsDao(CommentsDao commentsDao) {
		this.commentsDao = commentsDao;
	}
	
	public List<Comment> getCommentsForArticle(int id) {
		return this.commentsDao.getCommentsByArticleId(id);
	}

	public void addNewComment(Comment comment) {
		this.commentsDao.create(comment);
	}

	public void deleteByArticleId(int id) {
		this.commentsDao.deleteByArticleId(id);
		
	}
}
