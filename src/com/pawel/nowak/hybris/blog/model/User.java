package com.pawel.nowak.hybris.blog.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class User {
	
	@NotBlank(message = "Username cannot be empty.")
	@Size(min = 8, max = 20, message="Username must be between 8 and 20 characters.")
	@Pattern(regexp = "^\\w{8,}$", message="Username can only consist of letters, numbers and the underscore character.")
	private String username;
	
	@NotBlank(message = "Email cannot be empty.")
	//@Pattern(, message="This is not appear to be valid email address.")
	private String email;
	
	@NotBlank(message = "Password cannot be empty.")
	@Size(min = 8, max = 20, message = "Password must be between 8 and 20 characters")
	@Pattern(regexp = "^\\S+$", message = "Password cannot contain \" \"." )
	private String password;
	
	private boolean enabled = false;
	
	private String authority;
	
	public User() {
	}
	
	public User(String username, String email, String password,
			boolean enabled, String authority) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.enabled = enabled;
		this.authority = authority;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
}
