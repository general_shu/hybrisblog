package com.pawel.nowak.hybris.blog.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class Article {
	private int id;
	private String author;
	@NotNull
	@Size(max=2000, message="Text cannot be longer than 2000 characters.")
	private String text;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date date;
	@NotNull
	private String title;
	@NotNull
	private String keyWords;

	public Article() {
		super();
	}
	
	public Article(String author, String text, Date date, String title, String keyWords) {
		super();
		this.author = author;
		this.text = text;
		this.date = date;
		this.title = title;
		this.keyWords = keyWords;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", author=" + author + ", text=" + text
				+ ", date=" + date + ", title=" + title + ", keyWords="
				+ keyWords + "]";
	}
}
