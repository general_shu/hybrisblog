package com.pawel.nowak.hybris.blog.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class DateRange {
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date startingDate;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date endingDate;
	
	public DateRange() {
		super();
	}

	public DateRange(Date startingDate, Date endingDate) {
		super();
		this.startingDate = startingDate;
		this.endingDate = endingDate;
	}
	
	public Date getStartingDate() {
		return startingDate;
	}
	
	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}
	
	public Date getEndingDate() {
		return endingDate;
	}
	
	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}
}
