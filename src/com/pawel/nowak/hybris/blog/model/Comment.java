package com.pawel.nowak.hybris.blog.model;

import java.util.Date;

import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class Comment {
	private String author;
	@Size(max = 200, message = "Your comment cannot be longer than 200 characters.")
	private String text;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date date;
	private int articleId;
	
	public Comment() {
		super();
	}
	
	public Comment(String author, String text, Date date, int articleId) {
		super();
		this.author = author;
		this.text = text;
		this.date = date;
		this.articleId = articleId;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}
}
