<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>hybris Blog</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="http://getbootstrap.com/examples/blog/blog.css" />

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

</head>

<body>

	<div class="blog-masthead">
		<div class="container">
			<nav class="blog-nav">
				<a class="blog-nav-item active"
					href="${pageContext.request.contextPath}/">Home</a>
				<sec:authorize access="isAnonymous()">
					<a class="blog-nav-item"
						href="${pageContext.request.contextPath}/login">Log in</a>
				</sec:authorize>
				<sec:authorize access="isAnonymous()">
					<a class="blog-nav-item"
						href="${pageContext.request.contextPath}/register">Register</a>
				</sec:authorize>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<a class="blog-nav-item"
						href="${pageContext.request.contextPath}/admin">Admin</a>
				</sec:authorize>
			</nav>
		</div>
	</div>

	<div class="container">

		<div class="blog-header">
			<h1 class="blog-title">hybris Blog</h1>
			<p class="lead blog-description">Blog created for recrutation
				step for hybris Software.</p>
		</div>

		<div class="row">
			<div class="col-sm-8 blog-main">
				<p class="blog-post-meta">You don't have permission to access
					this page!</p>
				<p class="blog-post-meta">
					<a href="${pageContext.request.contextPath}/">Go back to main
						page!</a>
				</p>
			</div>
			<!-- /.blog-main -->
			<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
				<div>
					<sec:authorize var="loggedIn" access="isAuthenticated()">
						<li><a href="#">You're logged as: <%=request.getUserPrincipal().getName()%>
						</a></li>
					</sec:authorize>
					<div>
						<sec:authorize var="loggedIn" access="isAuthenticated()">
							<li><a href="<c:url value='/j_spring_security_logout'/>">Log
									out </a></li>
						</sec:authorize>
					</div>
				</div>
				<div class="sidebar-module sidebar-module-inset">
					<h4>About</h4>
					<p>Hello! My name is Pawel Nowak. I made this blog using Spring
						Framework for hybris Software recrutation step.</p>
				</div>
				<div>
					<sf:form class="form-horizontal" method="post"
						action="${pageContext.request.contextPath}/filter"
						commandName="dateRange">
						<fieldset>
							<legend>Filter articles using date:</legend>
							<div class="form-group">
								<label class="col-md-4 control-label article-date-picker"
									for="startingDate">From:</label>
								<div class="col-md-8">
									<input id="startingDate" name="startingDate" type="text"
										placeholder="Date" class="form-control input-md" required="">
									<span class="help-block"><sf:errors path="startingDate"
											cssClass="error"></sf:errors></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label article-date-picker"
									for="endingDate">To:</label>
								<div class="col-md-8">
									<input id="endingDate" name="endingDate" type="text"
										placeholder="Date" class="form-control input-md" required="">
									<span class="help-block"><sf:errors path="endingDate"
											cssClass="error"></sf:errors></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="singlebutton"></label>
								<div class="col-md-4">
									<input value="Filter" type="submit" class="btn btn-primary" />
								</div>
							</div>
						</fieldset>

					</sf:form>
				</div>
				<div>
					<sf:form class="form-horizontal" method="get"
						action="${pageContext.request.contextPath}/search"
						commandName="keyWords">
						<fieldset>
							<legend>Search for articles:</legend>
							<div class="form-group">
								<label class="col-md-4 control-label article-date-picker"
									for="startingDate">Search:</label>
								<div class="col-md-8">
									<input id="keyWords" name="keyWords" type="text"
										placeholder="Search" class="form-control input-md" required="">
									<span class="help-block"><sf:errors path="keyWords"
											cssClass="error"></sf:errors></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="singlebutton"></label>
								<div class="col-md-4">
									<input value="Search" type="submit" class="btn btn-primary" />
								</div>
							</div>
						</fieldset>

					</sf:form>
				</div>
				<div class="sidebar-module">
					<h4>Elsewhere</h4>
					<ol class="list-unstyled">
						<li><a href="#">GitHub</a></li>
						<li><a href="#">Twitter</a></li>
						<li><a href="#">Facebook</a></li>
					</ol>
				</div>
			</div>
			<!-- /.blog-sidebar -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

	<footer class="blog-footer">
		<p>
			Blog template built for <a href="http://getbootstrap.com">Bootstrap</a>
			by <a href="https://twitter.com/mdo">@mdo</a>.
		</p>
		<p>
			<a href="#">Back to top</a>
		</p>
	</footer>


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
	<script
		src="https://raw.githubusercontent.com/twbs/bootstrap/master/docs/assets/js/ie-emulation-modes-warning.js"></script>
	<script>
		$(function() {
			$('#startingDate').datepicker({
				format : 'mm-dd-yyyy'
			});
		});

		$(function() {
			$('#endingDate').datepicker({
				format : 'mm-dd-yyyy'
			});
		});
	</script>
</body>
</html>
