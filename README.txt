READ ME
If you want to run the application properly please follow this steps:
1. Create mySQL database using included dump .sql file in mysqldump directory.
2. Include context.xml in your Tomcat server for proper connection with your database.
3. All the .js and .css files are being downloaded from CDN.

I hope you will be satisfied with my code.

Have a nice day. 